import math
import functools

coef = 2.7
height = 4
width = 12
blockSize = 48

# new
# def startNewGame ():
#    main()

# canvas = document.getElementById("canvas");
# ctx = canvas.getContext('2d');


class Block:
    def __init__(self, horizontal, vertical, winConds=[], isFree=True):
        self.horizontal = horizontal
        self.vertical = vertical
        self.winConds = winConds
        self.isFree = isFree

    def addWinConditionDependency(self, winCond):
        self.winConds.append(winCond)

    def importance(self):
        return functools.reduce(lambda acc, winCond: acc + winCond.importance(), self.winConds, 0)

    # new
    def disableWinConds(self):
        for winCond in self.winConds:
            winCond.cantWinAnymore()

    # new
    def checkWins(self):
        for winCond in self.winConds:
            winCond.checkWin()

    """
    # new
    def checkLocalyMostImportant (): 
        maxImportance = 0;
        actBlockWithMaxImportance;
        for i in range(self.winConds.length):
            for j in range(self.winConds[i].blocks.length):
                if (maxImportance < self.winConds[i].blocks[j].importance() and self.winConds[i].blocks[j].isFree)): 
                    maxImportance = self.winConds[i].blocks[j].importance();
                    actBlockWithMaxImportance = self.winConds[i].blocks[j];
                    print(maxImportance);
        print(maxImportance);
        return actBlockWithMaxImportance; 
    """


class Blocks:
    def __init__(self):
        self.blocks = {}

    def get(self, x, y):
        return self.blocks[x, y]

    def set(self, x, y, value):
        self.blocks[x, y] = value

    def mostImportant(self):
        # FIXME it could be done easier in functional style
        current = None
        currentValue = None
        for idxPair in self.blocks:
            block = self.blocks[idxPair[0], idxPair[1]]
            value = block.importance()
            isFree = block.isFree
            if current is None or value > currentValue and isFree:
                current = block
                currentValue = value
        # print(currentValue)
        return current

    def draw(self):
        pass

    def blocksBetween(self, start, end):
        minHorizontal = min(start.horizontal, end.horizontal)
        minVertical = min(start.vertical, end.vertical)
        maxHorizontal = max(start.horizontal, end.horizontal)
        maxVertical = max(start.vertical, end.vertical)

        horizontalDelta = maxHorizontal - minHorizontal
        verticalDelta = maxVertical - minVertical

        # check if valid, must be in one of 4 directions
        if horizontalDelta == 0:  # horizontal line
            assert 0 < verticalDelta < width
        elif verticalDelta == 0:  # vertical line
            assert 0 < horizontalDelta < height
        else:  # diagonal line
            assert horizontalDelta == verticalDelta

        blockRange = []
        expectedLength = max(horizontalDelta, verticalDelta) + 1

        if verticalDelta == 0:
            # vertical line
            for x in range(minHorizontal, maxHorizontal):
                blockRange.append(self.blocks[x, minVertical])
        elif horizontalDelta == 0:
            # horizontal line
            for y in range(minVertical, maxVertical):
                blockRange.append(self.blocks[minHorizontal, y])
        else:
            # diagonal line
            assert horizontalDelta == verticalDelta
            isUpward = start.vertical > end.vertical
            isBackward = start.horizontal > end.horizontal
            stepX = -1 if isBackward else 1
            stepY = -1 if isUpward else 1

            for x, y in zip(range(start.horizontal, end.horizontal, stepX), range(start.vertical, end.vertical, stepY)):
                blockRange.append(self.blocks[x, y])

            blockRange.append(self.blocks[end.horizontal, end.vertical])  # push the last one (bc. inclusive ranges)

        # sanity checks
        assert len(blockRange) == expectedLength
        for block in blockRange:
            assert block is not None  # each must exist

        return blockRange

    # private utilities
    # TODO: Generalize for other shapes
    def _getBlockIndexesInColumn(self, colIdx):
        acc = []
        for block in self.blocks:
            if block[0] == colIdx:
                acc.append(block)
        return acc

    def _getBlockIndexesInRow(self, rowIdx):
        acc = []
        for block in self.blocks:
            if block[1] == rowIdx:
                acc.append(block)
        # print(acc)
        return acc

    def blocksInRow(self, rowIdx):
        # new
        blockIdxs = self._getBlockIndexesInRow(rowIdx)
        acc = []
        for idxPair in blockIdxs:
            acc.append(self.blocks[idxPair[0], idxPair[1]])
        blockRange = acc
        return blockRange

        #         blockRange = functools.reduce(
        #             lambda acc, idxPair: acc.append(self.blocks[idxPair[0], idxPair[1]]), blockIdxs, [])

    def blocksInColumn(self, colIdx):
        blockIdxs = self._getBlockIndexesInRow(colIdx)
        acc = []
        for idxPair in blockIdxs:
            acc.append(self.blocks[idxPair[0], idxPair[1]])
        blockRange = acc
        return blockRange

    def blocksInMajorDiagonal(self, diagIdx):
        assert 0 <= diagIdx <= width + height - 4, "diagIdx not in range"
        # magic begins
        startx = diagIdx - (height - 2)
        starty = (height - 1)
        endx = startx + (height - 1)
        endy = 0
        # print(f'{startx} {starty} {endx} {endy}')
        # if we run out of space, trunk the diag:
        while startx < 0:
            startx += 1
            starty -= 1

        while endx > width - 1:
            endx -= 1
            endy += 1

        # print(f'{startx} {starty} {endx} {endy}')
        blockRange = self.blocksBetween(self.blocks[startx, starty], self.blocks[endx, endy])
        return blockRange

    def blocksInMinorDiagonal(self, diagIdx):
        assert 0 <= diagIdx <= width + height - 4, "diagIdx not in range"
        # magic begins
        startx = diagIdx - (height - 3)
        starty = 0
        endx = startx + (height - 1)
        endy = (height - 1)
        # if we run out of space, trunk the diag:
        while startx < 0:
            startx += 1
            starty += 1

        while endx > width - 1:
            endx -= 1
            endy -= 1

        # print(f'{startx} {starty} {endx} {endy}')
        blockRange = self.blocksBetween(self.blocks[startx, starty], self.blocks[endx, endy])
        return blockRange


class WinCondition:
    def __init__(self, blocks, couldWin=True):
        self.blocks = blocks
        self.couldWin = couldWin
        for block in self.blocks:
            block.addWinConditionDependency(self)

    def importance(self):
        return math.pow(coef, 7 - self.lives()) if self.couldWin else 0

    # new
    def cantWinAnymore(self):
        self.couldWin = False

    # new
    def checkWin(self):
        if self.lives() == 0 and self.couldWin:
            for block in self.blocks:
                block.draw("red")
            self.blocks.addWinConditionDependency(self)
            print("White player won the game!")

    def lives(self):
        return functools.reduce(lambda acc, block: acc + block.isFree, self.blocks, 0)


class WinConditions:
    def __init__(self, winConditions=[]):
        self.winConditions = winConditions


# creates a dictionary which maps [x, y]
# coordinates to Block instances
# FIXME refactor, looks ugly
def makeBlocks():
    blocks = Blocks()
    for i in range(width):
        for j in range(height):
            blocks.set(i, j, Block(i, j))
    return blocks


def makeWinConditionsFor(blocks):
    def makeWinConditionsForBlockRange(blockRange):
        # if the row == small enough, the whole row == a single WinCondition
        if len(blockRange) < 5:
            return [WinCondition(blockRange)]

        # otherwise we begin and end with a 4 long WinCondition, and each 6 long sub block-range
        winConditions = [WinCondition(blockRange)][0:4]

        # each 7 long subranges (if any)
        subrangeCount = max(len(blockRange) - 8, 0)
        for nth in range(subrangeCount):
            winConditions.append([WinCondition(blockRange)][nth:nth + 7])

        winConditions.append([WinCondition(blockRange)][-4:])  # last 4
        # new
        # winConditions = applyExtraRules(winConditions, blockRange);
        return winConditions

    def winCondGenerator(times, subrangeGeneratorFunc):
        acc = []
        for nth in range(times):
            blockRange = subrangeGeneratorFunc(nth)
            acc += makeWinConditionsForBlockRange(blockRange)
        return acc

    winConditions = [] + \
                    winCondGenerator(height, lambda idx: blocks.blocksInRow(idx)) + \
                    winCondGenerator(width, lambda idx: blocks.blocksInColumn(idx)) + \
                    winCondGenerator(width + height - 4, lambda idx: blocks.blocksInMajorDiagonal(idx)) + \
                    winCondGenerator(width + height - 4, lambda idx: blocks.blocksInMinorDiagonal(idx))
    return WinConditions(winConditions)


def main():
    blocks = makeBlocks()
    winConds = makeWinConditionsFor(blocks)
    blocks.draw()
    whiteIsOnTurn = True
    # print(winConds)

    def machineSteps():
        # searches for local most important, if finds nothing, searches globally
        # bestBlock = blocks.get(x, y).checkLocalyMostImportant()
        # if bestBlock is None:
        bestBlock = blocks.mostImportant()
        bestBlock.isFree = False
        # bestBlock.draw('black')
        bestBlock.disableWinConds()

    for stepId in range(blockSize):
        machineSteps()
        whiteIsOnTurn = -whiteIsOnTurn


if __name__ == "__main__":
    main()
